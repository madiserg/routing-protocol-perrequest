package common.exception

import spray.http.{StatusCode, StatusCodes}
//TODO: change "created by" comments
/**
 * Common exceptions
 *
 * Created by Madi
 */

/**
 * Base Response Exception
 */
trait ApiException extends RuntimeException {

  /**
   * Http status
   */
  def status : StatusCode

  /**
   * Exception code
   */
  def errorCode : ErrorCode

  /**
   * Exception message
   */
  def message: Option[String]

  /**
   * Returns error info
   *
   * @param errorLocaleContext
   * @return
   */
  def getErrorInfo(errorLocaleContext : Option[ErrorLocaleContext]) : ErrorInfo = {
    val fullCode = getFullCode
    val errorUrl = "docs/" + fullCode
    val localizedMessage : Option[String] = errorLocaleContext match {
      case Some(context) => Some(context.getLocalizedMessage(fullCode))
      case _ => message
    }

    val developerMessage : Option[String] = getMessage match {
      case null => message
      case _ => Some(getMessage)
    }

    ErrorInfo(Some(status.intValue), Some(errorCode.series.series), Some(fullCode), localizedMessage, developerMessage, Some(errorUrl))
  }

  /**
   * Builds full error code according
   *
   * @return Long which represents error code
   */
  def getFullCode : Long = (status.intValue * 100 + errorCode.series.series) * 1000 + errorCode.code
}

/**
 * Error code
 */
trait ErrorCode {
  def series : ErrorSeries
  def code : Int
}

/**
 * Error series
 */
trait ErrorSeries {
  def series : Int
}

/**
 * Error locale context
 */
trait ErrorLocaleContext {
  def getLocalizedMessage(fullErrorCode: Long) : String
}

/**
 * Represents error info
 *
 * @param status            - http status
 * @param series            - exception series
 * @param code              - exception code
 * @param message           - error message (localized)
 * @param developerMessage  - message for developer
 * @param moreInfo          - some more info
 */
case class ErrorInfo(status:            Option[Int],
                     series:            Option[Int],
                     code:              Option[Long],
                     message:           Option[String],
                     developerMessage:  Option[String],
                     moreInfo:          Option[String]) {

}

/**
 * Not found exception (404)
 *
 * @param errorCode - ErrorCode
 * @param message   - optional exception message
 */
case class NotFoundException(override val errorCode : ErrorCode,
                             override val message   : Option[String] = None) extends ApiException {
  override val status: StatusCode = StatusCodes.NotFound
}

/**
 * Bad request exception (400)
 *
 * @param errorCode - ErrorCode
 * @param message   - optional exception message
 */
case class BadRequestException(override val errorCode : ErrorCode,
                               override val message   : Option[String] = None) extends ApiException {
  override val status: StatusCode = StatusCodes.BadRequest
}

/**
 * Forbidden exception (403)
 *
 * @param errorCode - ErrorCode
 * @param message   - optional exception message
 */
case class ForbiddenErrorException(override val errorCode : ErrorCode,
                                   override val message   : Option[String] = None) extends ApiException {
  override val status: StatusCode = StatusCodes.Forbidden
}

/**
 * Internal server error exception (500)
 *
 * @param errorCode - ErrorCode
 * @param message   - optional exception message
 */
case class ServerErrorRequestException(override val errorCode : ErrorCode,
                                       override val message   : Option[String] = None) extends ApiException {
  override val status: StatusCode = StatusCodes.InternalServerError
}

/**
 * Unauthorized exception (401)
 *
 * @param errorCode - ErrorCode
 * @param message   - optional exception message
 */
case class UnauthorizedErrorException(override val errorCode  : ErrorCode,
                                      override val message    : Option[String] = None) extends ApiException {
  override val status: StatusCode = StatusCodes.Unauthorized
}

