package routing.protocol.core

import common.exception.{ErrorInfo, ApiException}
import spray.http.{HttpEntity, StatusCode}
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling.{CollectingMarshallingContext, Marshaller, MetaMarshallers, ToResponseMarshaller}
import spray.json._

/**
 *
 * Created by Madi
 */

/**
 * Case class that represents Error inside application
 * @param code Status code that will be returned in response
 * @param entity Response entity
 */
case class ErrorResponseException(code: StatusCode, entity: HttpEntity) extends Throwable

/**
 * Main trait for marshalling support
 */
trait Marshalling extends DefaultJsonProtocol with SprayJsonSupport with MetaMarshallers {

  /*
   * NOTE: if in the future you need to print null values (e.g. "message":null)
   *       then uncomment this block and comment jsonFormat6(ErrorInfo) line
   */
//  implicit object ErrorInfoJsonFormat extends RootJsonFormat[ErrorInfo] {
//
//    override def write(errorInfo: ErrorInfo): JsValue = JsObject(
//      "status"            -> (if (errorInfo.status.isDefined)           { JsNumber(errorInfo.status.get)            } else { JsNull }),
//      "series"            -> (if (errorInfo.series.isDefined)           { JsNumber(errorInfo.series.get)            } else { JsNull }),
//      "code"              -> (if (errorInfo.code.isDefined)             { JsNumber(errorInfo.code.get)              } else { JsNull }),
//      "message"           -> (if (errorInfo.message.isDefined)          { JsString(errorInfo.message.get)           } else { JsNull }),
//      "developerMessage"  -> (if (errorInfo.developerMessage.isDefined) { JsString(errorInfo.developerMessage.get)  } else { JsNull }),
//      "moreInfo"          -> (if (errorInfo.moreInfo.isDefined)         { JsString(errorInfo.moreInfo.get)          } else { JsNull })
//    )
//
//    def read(value: JsValue): ErrorInfo = {
//      value.convertTo[ErrorInfo]
//    }
//  }

  /*
   * NOTE: if printing null values is not required then this this block as it's shorter
   */
  implicit val ErrorInfoJsonFormat = jsonFormat6(ErrorInfo)

  /**
   * Function for handling errors when API returns Left(ERROR) or Right(Response)
   * For more information how eitherCustomMarshaller works check Jan Machacek entry about errors and failures:
   * http://www.cakesolutions.net/teamblogs/2012/12/10/errors-failures-im-a-teapot
   */
  implicit def eitherCustomMarshaller[A, B](implicit ma: Marshaller[A], mb: Marshaller[B]): ToResponseMarshaller[Either[A, B]] =
    Marshaller[Either[A, B]] { (value, ctx) =>
      value match {
        case Left(ex:ApiException) =>
          ctx.handleError(ex)
        case Left(a) =>
          val mc = new CollectingMarshallingContext()
          ma(a, mc)
          ctx.handleError(ErrorResponseException(200, mc.entity))
        case Right(b) =>
          (200, mb(b, ctx))
      }
    }

}