package routing.protocol.core

import java.util.Locale
import java.util.concurrent.TimeUnit

import akka.actor.ActorContext
import akka.util.Timeout
import common.exception.ApiException
import routing.protocol.domain.exception.RoutingProtocolErrorLocaleContext
import spray.http.{ContentTypes, HttpEntity, StatusCodes}
import spray.json._
import spray.routing._
import spray.util.LoggingContext

import scala.util.control.NonFatal

/**
 * Created by Madi
 */
/**
 * Http Actor that handles URL calls
 */
//class ApplicationApiActor(route: Route) extends HttpServiceActor with CustomErrorHandler {
//
//  def getRejectionMessage(message: String, language: String) : String = {
//    println("Rejection error")
//    val userMessage = LocalizedMessages.getMessage(new Locale(language), message.substring(20))
//    val code = "400"+RoutingErrorCodes.INPUT_PARAM_VALIDATION_ERROR.series.series+RoutingErrorCodes.INPUT_PARAM_VALIDATION_ERROR.code
//    s"""{"series":${RoutingErrorSeries.REQUEST_ERROR.series}, "code":${code}, "moreInfo":"docs/${RoutingErrorCodes.INPUT_PARAM_VALIDATION_ERROR}", "status":400, "developerMessage": "${message}", "message":"${userMessage}"}"""
//  }
//
//  implicit val customRejectionHandler = RejectionHandler {
//
//    case ValidationRejection(message, _) :: _ =>
//      complete(StatusCodes.BadRequest, HttpEntity(ContentTypes.`application/json`, s"""{"developerMessage": "${message}", "message":"${message.substring(20)}"}"""))
//
//    case MalformedRequestContentRejection(message, _) :: _ =>
//      complete(StatusCodes.BadRequest, HttpEntity(ContentTypes.`application/json`, s"""{"developerMessage": "${message}", "message":"${message.substring(20)}"}"""))
//  }
//
////  override def receive: Receive = runRoute(route)(customExceptionHandler, customRejectionHandler, actorRefFactory,
////    RoutingSettings.default(actorRefFactory), LoggingContext.fromActorContext(actorRefFactory))
//
//}



/**
 * Use to configure when application should throw Timeout exception
 */
trait DefaultTimeout {
  implicit val timeout = new Timeout(300, TimeUnit.SECONDS)
}

/**
 * Custom error handler, If Api call can return OK or One error You can use eitherCustomErrorMarshaller to configure
 * what type of StatusCode should be returned
 */
trait CustomErrorHandler extends Marshalling {

  def getLanguageFromHeader(context: RequestContext) : String = {

    context.request.headers.find(_.name.equalsIgnoreCase("Accept-Language")) match {
      case Some(header) => header.value
      case _ => "ru"
    }

  }

  implicit def customExceptionHandler(implicit log: LoggingContext, context: ActorContext): ExceptionHandler =
    ExceptionHandler.apply {
      case NonFatal(ErrorResponseException(statusCode, entity)) =>

        log.error(s"Application return expected error status code ${statusCode} with entity ${entity} ")
        ctx => ctx.complete((statusCode, entity))

      case e: ApiException => ctx =>

        val language = getLanguageFromHeader(ctx)
        val errorInfo = e.getErrorInfo(Some(new RoutingProtocolErrorLocaleContext(new Locale(language))))
        val errorInfoJson = errorInfo.toJson

        log.error(s"Application return expected error status code [${language}] ${e.status} with entity ${errorInfoJson} ")

        ctx.complete((e.status, HttpEntity(ContentTypes.`application/json`, errorInfoJson.toString)))

      case NonFatal(e) =>
        log.error(s"Application return unexpected error with exception ${e}")
        ctx => ctx.complete(StatusCodes.InternalServerError)
    }
}
