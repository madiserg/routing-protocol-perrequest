package routing.protocol.core

import akka.actor.Actor
import com.typesafe.config.ConfigFactory

/**
 * Created by Madi
 */
/**
 * This actor:
 * - when receive Startup message it creates actors that will handle our requests
 * - when receive Shutdown message it stops all actors from context
 */
case class Startup()
case class Shutdown()

class ApplicationActor extends Actor {

  def receive: Receive = {
    case Startup() => {

      val config = ConfigFactory.load()
      val checkToken = config.getBoolean("prod")

//      val jwtService = new JwtService(checkToken)
//      val itemDao = new ItemDao
//      val merchantDao = new MerchantProtocolDao
//      val productDao = new ProductDao
//      val httpPipeline = new HttpPipeline()
//      val protocolService = new ProtocolService(productDao, httpPipeline)

//      context.actorOf(Props(classOf[RoutingActor], jwtService), "routing")
//      context.actorOf(Props(classOf[ProtocolActor], itemDao, merchantDao, protocolService), "protocol")
//      context.actorOf(Props(classOf[ProtocolActor]), "protocol")

      sender ! true
    }
    case Shutdown() => {
      println(">>> Shutting down")
      context.children.foreach(ar => context.stop(ar))
    }
  }
}
