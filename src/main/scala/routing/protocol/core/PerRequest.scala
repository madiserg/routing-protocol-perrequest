package routing.protocol.core

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{OneForOneStrategy, _}
import kz.dar.eco.messages.core.{DomainMessage, RequestMessageType}
import org.slf4j.LoggerFactory
import routing.protocol.Serializers
import routing.protocol.core.PerRequest.{WithActorRef, WithProps}
import routing.protocol.exceptions.LocalizedMessages
import spray.http.StatusCodes._
import spray.http.{HttpHeaders, HttpResponse, StatusCode}
import spray.httpx.Json4sSupport
import spray.routing.RequestContext

import scala.concurrent.duration._

//case class ErrorInfo(status:            Option[Int],
//                     series:            Option[Int],
//                     code:              Option[Long],
//                     message:           Option[String],
//                     developerMessage:  Option[String],
//                     moreInfo:          Option[String])

trait ApiMessage[T] extends DomainMessage[T] {
  override val routingKey = ""
  override val headers: Map[String, String]
  val messageType: RequestMessageType
}

trait PerRequest extends Actor with ActorLogging with Json4sSupport with Serializers {

  import context._
  val logger = LoggerFactory.getLogger(this.getClass)
  val json4sFormats = formats

  def r: RequestContext
  def target: ActorRef
  def message: DomainMessage[_]
//  def message: RequestMessage

  logger.debug("--> PerRequest actor: "+self.path.name.toString)
  logger.debug("--> PerRequest actor: "+self.path.toString)
  logger.debug("--> PerRequest actor: "+self.toString())

  logger.debug("--> PerRequest target: "+target.toString())
//  println("target: "+target)
  setReceiveTimeout(60.seconds)
  target ! message

  def receive = {
    case res: HttpResponse =>
//      complete(OK, None)
//      val response = new String(res.entity.data.toByteArray,"UTF-8")
      val response = new String(res.entity.data.toByteArray)
//      response.replaceAll("[\\n\\]","");
//        response = response.replaceAll("\\\\"," ");

//      response.filter{
//
//        _ match {
//          case '\n' =>
//            println("csae n")
//            true
//          case c @ _ =>
//            println("case _ "+c)
//            false
//        }
//      }
      logger.debug("PerRequest received response: "+response)
//      complete(OK, None)
      complete(OK, response)

    case e: common.exception.ApiException =>
      logger.debug("ApiException "+e)
      completeWithError(e)

    case m @ _ =>
      logger.debug("default case "+m)
//    case _ =>
//      println("asf")
//    case Some(res) =>
//      complete(OK, None)
//
////    case l @ Left =>
//    case Some(BadRequestException) =>
//      println("left")
//    case res @ _ =>
//      println("per request bad request: "+ res)


//    case res: Accepted          => complete(StatusCode.int2StatusCode(res.status), res)
//    case res: SeqEntity[_]      => complete(OK, res.entities)
//    case res: EmptyResponse     => complete(NoContent, None)
//    case res: Seq[DomainEntity] => complete(OK, res)
//    case res: DomainEntity      => complete(OK, res)
//    case ReceiveTimeout         => completeWithError(GatewayTimeoutErrorException(WalletErrorCodes.TIMEOUT_ERROR(WalletErrorSeries.WALLET_API)))
//    case e: ApiException        => completeWithError(e)
//    case e: ErrorInfo           => complete(StatusCode.int2StatusCode(e.status.get), e)
  }

  /**
    * Completes request with the given status code and serializable object
    *
    * @param status - request status code
    * @param obj    - serializable object to send
    */
  def complete[T <: AnyRef](status: StatusCode, obj: T) = {
    r.complete(status, obj)
    stop(self)
  }

  /**
   * Completes request with error
    *
   * @param apiException
   */

  def completeWithError(apiException: common.exception.ApiException) = {
    val language = RequestUtil.getLanguageFromHeader(r)
//    val errorInfo = apiException.getErrorInfo(ErrorLocaleContextFactory.getContextForLocale(new Locale(language)))

//    ErrorInfo(Some(status.intValue), Some(errorCode.series.series), Some(fullCode), localizedMessage, developerMessage, Some(errorUrl))

    val fullCode : Long = ((apiException.status.intValue * 100 + apiException.errorCode.series.series) * 1000 + apiException.errorCode.code)

    val errorInfo = common.exception.ErrorInfo(
      Some(apiException.status.intValue),
      Some(apiException.errorCode.series.series),
      Some(fullCode),
      Some(fullCode.toString),
      apiException.message,
      Some("docs/"+fullCode)
    )
    log.error(s"Application return expected error status code [${language}] ${apiException.status} with entity ${errorInfo} ")
    complete(apiException.status, errorInfo)
    stop(self)
  }

  override val supervisorStrategy =
    OneForOneStrategy() {

      /**
       * Catching Api Exceptions
       */
      case e: common.exception.ApiException => {

        completeWithError(e)

        Stop
      }

      /**
       * Catching any other exceptions
       */
//      case e => {
//
//        val error = ServerErrorRequestException(WalletErrorCodes.INTERNAL_SERVER_ERROR(WalletErrorSeries.WALLET_API), Some(e.getMessage))
//        completeWithError(error)
//
//        Stop
//
//      }
    }
}

object PerRequest {

  case class WithActorRef(r: RequestContext, target: ActorRef, message: DomainMessage[_]) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, message: DomainMessage[_]) extends PerRequest {
    logger.debug("withProps")
    lazy val target = context.actorOf(props)
  }
//  case class WithActorRef(r: RequestContext, target: ActorRef, message: RequestMessage) extends PerRequest
//
//  case class WithProps(r: RequestContext, props: Props, message: RequestMessage) extends PerRequest {
//    lazy val target = context.actorOf(props)
//  }

}

trait PerRequestCreator {
  this: Actor =>

//  def perRequest(r: RequestContext, target: ActorRef, message: RequestMessage) =
//    context.actorOf(Props(new WithActorRef(r, target, message)))
//
//  def perRequest(r: RequestContext, props: Props, message: RequestMessage) = {
//    context.actorOf(Props(new WithProps(r, props, message)))
//  }
  def perRequest(r: RequestContext, target: ActorRef, message: DomainMessage[_]) = {

//  logger.debug("perRequest withActorRef")
  context.actorOf(Props(new WithActorRef(r, target, message)))
}

  def perRequest(r: RequestContext, props: Props, message: DomainMessage[_]) = {
//    logger.debug("perRequest withProps")
    context.actorOf(Props(new WithProps(r, props, message)))
  }

}

object RequestUtil {

  /**
    * Returns requested language from header.
    * If not presented, then default language is returned
    *
    * @param context - request context
    * @return language as string
    */
  def getLanguageFromHeader(context: RequestContext) : String = {

    val defaultLanguage = "ru"

    context.request.headers.find(_.name.equalsIgnoreCase("Accept-Language")) match {

      case Some(header) =>

        val lan : Option[String] = header match {

          case HttpHeaders.`Accept-Language`(languages) =>

            languages.find(_.primaryTag != "null").map(_.primaryTag)

          case _ =>

            LocalizedMessages.locales.find(header.value.indexOf(_) >= 0)

        }

        lan match {
          case Some(l) => l
          case _ => defaultLanguage
        }

      case _ => defaultLanguage

    }

  }

}