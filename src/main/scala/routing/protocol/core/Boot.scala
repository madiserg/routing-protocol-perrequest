package routing.protocol.core

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.typesafe.config.ConfigFactory
import kz.dar.eco.domain.core.{EcoService, Endpoint}
import routing.protocol.amqp.AMQPConnectionCustom
import routing.protocol.api.RoutingProtocolAPI
import spray.can.Http

import scala.util.Try

/**
  * Created by madi on 8/31/16.
  */

/**
  * Main application launcher.
  * - defines actor system for our application
  * - creates server instance
  * - add shutdown hook for actor system
  */
object Boot extends App {


  implicit val system = ActorSystem("eco-routing-protocol-perRequest")

//  def main(args: Array[String]): Unit = {
//
//    class Application(val actorSystem: ActorSystem) extends BootSystem with Api with ServerIO
//
//    new Application(system)
//
//    sys.addShutdownHook(system.shutdown())
//  }
  val config = ConfigFactory.load()

  def getStringConfigKey(key: String) : String = {
    try {
      sys.env(key)
    } catch {
      case e: NoSuchElementException =>
        config.getString(key)
    }
  }

  def getIntConfigKey(key: String) : Int = {
    try {
      sys.env(key).toInt
    } catch {
      case e: NoSuchElementException =>
        config.getInt(key)
    }
  }

  //commenting out rmq implementation
//  val replyKey = getStringConfigKey("amqp.consumer.reply.key")

  val amqpConnection = new AMQPConnectionCustom(
    getStringConfigKey("amqp.connection.host"),
    getIntConfigKey("amqp.connection.port"),
    getStringConfigKey("amqp.connection.user"),
    getStringConfigKey("amqp.connection.password"),
    getIntConfigKey("amqp.connection.reconnectionDelay"),
    Try{getStringConfigKey("amqp.connection.virtualHost")}.toOption
  )
//
  val hostname = config.getString("host")
  val ecoRoutingService = EcoService(
    config.getString("amqp.service.system"),
    config.getString("amqp.service.subSystem"),
    config.getString("amqp.service.microService")
  )
  implicit val ecoRoutingProtocol = Endpoint(hostname, ecoRoutingService)

  //incoming message gate
  val inboundGate = config.getString("amqp.endpoints.eco.routingProtocol.inExchange")

  //outgoing message gate
  val outboundGate = config.getString("amqp.endpoints.eco.routingProtocol.outExchange")


  //commenting out rmq implementation
  //we only need requestPulisher so we could publish http requests to rmq, and then consume it
  //it's a kludge
//  val requestPublisher = system.actorOf(RequestPublisherActor.props(amqpConnection, inboundGate))

  //commenting out rmq implementation
//  val responsePublisher = system.actorOf(RequestPublisherActor.props(amqpConnection, outboundGate))
//  println("--> responsePublisher: "+responsePublisher.toString())

  //commenting out rmq implementation
//  val routingListener = system.actorOf(RoutingAMQPListener.props(responsePublisher), "routing-amqp-listener")
  //

//  val readConsumer = new AMQPConsumer(amqpConnection, routingListener, "reply", queueName = Some(ecoRoutingProtocol.queue), keyName = Some(replyKey))
//commenting out rmq implementation
//  val readConsumer = new AMQPConsumer(amqpConnection, routingListener, "reply", queueName = None, keyName = Some(replyKey))
//commenting out rmq implementation
//  implicit val amqpPublisher = new AMQPPublisherImpl(amqpConnection)

  val serviceActor = system.actorOf(Props(new RoutingProtocolAPI(system.deadLetters)), name = "rest-routing")

  system.registerOnTermination {
    system.log.info("Routing Protocol shutdown.")
  }

  val host = config.getString("application.server.host")
  val port = config.getInt("application.server.port")

  IO(Http) ! Http.Bind(serviceActor, host, port = port)

  system.log.info(s"routing protocol started: ${host}:${port}")



}
