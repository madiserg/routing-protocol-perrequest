package routing.protocol.domain.form

import org.joda.time.DateTime

/**
 * Created by Madi
 */

case class UserInfo (id:        Long,
                     username:  Option[String],
                     email:     Option[String],
                     firstName: Option[String],
                     lastName:  Option[String],
                     ip:        Option[String]) {

  require(id > 0, "INVALID_USER_ID")
}

case class PaymentAmount(amount: BigDecimal, currency: String) {
  require(amount > 0, "INVALID_AMOUNT")
//  require(currency == "KZT" || currency == "RUR" || currency == "USD" || currency == "EUR", "INVALID_CURRENCY")
}

case class RegionValue(country: String,
                       city: String)

case class CheckRequestForm(productId:      Option[Long],
                            productCode:        Option[String],
                            userInfo:       UserInfo,
                            paymentAmount: PaymentAmount,
                            addData: Option[Map[String, String]],
                            srcUser: Option[User],
                            destUser: Option[User],
                            operationCreated: Option[DateTime],
                            operationDate: Option[DateTime],
                            operationId: Option[Long],
                            regionValue: Option[RegionValue],
                            keys: Option[Seq[String]],
                            values: Seq[String]) {
  require(productId.isDefined && productId.get > 0, "INVALID_PRODUCT_ID")
  require(values.length > 0, "INVALID_VALUES")
}
