package routing.protocol.domain.form

import org.joda.time.DateTime

/**
 * Created by Madi
 */
case class TransferConfirmForm(productId:      Long,
                               productCode:    String,
                               userInfo:       UserInfo,
                               paymentAmount: PaymentAmount,
                               addData: Map[String, String],
                               status: Option[String],
                               srcUser: Option[User],
                               destUser: Option[User],
                               operationCreated: Option[DateTime],
                               operationDate: Option[DateTime],
                               operationId: Option[Long],
                               regionValue: Option[RegionValue],
                               keys: Seq[String],
                               values: Seq[String])
