package routing.protocol.domain.form

import org.joda.time.DateTime

/**
 * Created by Madi
 */
case class ConfirmRequestForm(productId:      Option[Long],
                              productCode:    Option[String],
                              userInfo:       UserInfo,
                              paymentAmount: PaymentAmount,
                              addData: Option[Map[String, String]],
                              status: Option[String],
                              srcUser: Option[User],
                              destUser: Option[User],
                              operationCreated: Option[DateTime],
                              operationDate: Option[DateTime],
                              operationId: Option[Long],
                              regionValue: Option[RegionValue],
                              keys: Option[Seq[String]],
                              values: Seq[String]) {
  require(productId.isDefined && productId.get > 0, "INVALID_PRODUCT_ID")
  require(status.isDefined, "INVALID_STATUS")
  require(values.length > 0, "INVALID_VALUES")
}
