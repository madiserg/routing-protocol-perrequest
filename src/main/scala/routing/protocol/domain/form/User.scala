package routing.protocol.domain.form

/**
 * Created by Madi
 */
case class User(identifier: Option[Identifier],
                walletType: Option[String],
                iin: Option[String],
                name: Option[String],
                series: Option[Int],
                roleType: Option[String],
                accountType: Option[String],
                currency: Option[String],
                number: Option[String],
                state: Option[String],
                description: Option[String],
                id: Option[Long])

case class Identifier(id: Option[String], idspace: Option[String], paySystem: Option[String])