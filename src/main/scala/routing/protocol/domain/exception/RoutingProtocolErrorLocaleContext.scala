package routing.protocol.domain.exception


import java.util.Locale

import com.typesafe.config.{Config, ConfigException, ConfigFactory}
import common.exception.ErrorLocaleContext
import scala.collection.JavaConversions._

/**
 * Error messages provider
 *
 * Created by Madi
 */

/**
 * Returns error message for given error code(message key) and locale
 *
 * @param locale - message localization
 */
class RoutingProtocolErrorLocaleContext(locale: Locale) extends ErrorLocaleContext {

  /**
   * Returns localized message for the given error code(message key)
   *
   * @param fullErrorCode - error code (message key)
   * @return localized message
   */
  def getLocalizedMessage(fullErrorCode: Long) : String = {

    LocalizedMessages.getMessage(locale, fullErrorCode.toString)

  }

}

/**
 * Loads localized messages from config using TypeSave Conf lib
 */
object LocalizedMessages {

  /**
   * Available locales
   */
  private val locales : Seq[String] = ConfigFactory.load().getStringList("languages")


  /**
   * Configs for each locale
   */
  private val configs : Map[String, Config] = locales.map(locale => locale -> ConfigFactory.load("messages." + locale + ".properties")).toMap

  /**
   * Returns localized message
   *
   * @param locale  - message locale
   * @param key     - message key
   * @return localized message
   */
  def getMessage(locale: Locale, key: String): String= {

    val config : Config = configs.getOrElse(locale.getLanguage, configs.head._2)

    try {
      config.getString(key)
    } catch {
      case e: ConfigException => key
    }

  }
}