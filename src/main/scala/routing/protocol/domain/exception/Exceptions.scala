package routing.protocol.domain.exception

import common.exception.{ErrorSeries, ErrorCode}

object RoutingErrorCodes {

  /**
   * code <= 999
   */
  case object CHECK_ERROR extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 1
  }

  case object CONFIRM_ERROR extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 2
  }

  case object AUTHORIZATION_ERROR extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 3
  }

  case object PRODUCT_NOT_FOUND extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 4
  }

  case object MERCHANT_NOT_FOUND extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 5
  }

  case object MERCHANT_ERROR extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 6
  }

  case object INPUT_PARAM_VALIDATION_ERROR extends ErrorCode {
    override val series = RoutingErrorSeries.REQUEST_ERROR
    override val code = 401
  }

  case class CustomErrorCode(val series1: Int, override val code: Int) extends ErrorCode {
    override val series = RoutingErrorSeries.CUSTOM_ERROR(series1)
  }

}

object RoutingErrorSeries {
  /**
   * series <= 99
   */
  case object REQUEST_ERROR extends ErrorSeries {
    override val series = 6
  }

  case class CUSTOM_ERROR(series1: Int) extends ErrorSeries {
    override val series = series1
  }
}
