package routing.protocol.utils

import akka.actor.ActorContext
import routing.protocol.api.CustomJsonSupport
import routing.protocol.domain.form.{TransferCheckForm, TransferConfirmForm}
import spray.client.pipelining._
import spray.http._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by darik on 7/28/15.
 */

case class JWTObject(key: String)

class HttpPipeline(implicit val actorContext: ActorContext) extends CustomJsonSupport {

  val pipeline = addHeader("Accept", "application/json") ~> sendReceive

  def get(url: String) : Future[HttpResponse] = {
    pipeline { Get(url) }
  }

  def post(url: String, jwt: String, data: Map[String, String], stan: Option[String]): Future[HttpResponse] = {
    stan match {
      case Some(stan) => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("Authorization", jwt) ~> addHeader("stan", stan) ~> sendReceive
        pline { Post(url, data) }
      }
      case None => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("Authorization", jwt) ~> sendReceive
        pline { Post(url, data) }
      }
    }
  }

  def post(url: String, data: Map[String, String], stan: Option[String]): Future[HttpResponse] = {
    stan match {
      case Some(stan) => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("stan", stan) ~> sendReceive
        pline { Post(url, data) }
      }
      case None => {
        pipeline { Post(url, data) }
      }
    }
  }

  def post(url: String, jwt: String, request: TransferCheckForm, stan: Option[String]): Future[HttpResponse] = {
    stan match {
      case Some(stan) => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("Authorization", jwt) ~> addHeader("stan", stan) ~> sendReceive
        pline { Post(url, request) }
      }
      case None => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("Authorization", jwt) ~> sendReceive
        pline { Post(url, request) }
      }
    }
  }

  def post(url: String, jwt: String, request: TransferConfirmForm, stan: Option[String]): Future[HttpResponse] = {
    stan match {
      case Some(stan) => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("Authorization", jwt) ~> addHeader("stan", stan) ~> sendReceive
        pline { Post(url, request) }
      }
      case None => {
        val pline = addHeader("Accept", "application/json") ~> addHeader("Authorization", jwt) ~> sendReceive
        pline { Post(url, request) }
      }
    }
  }
}
