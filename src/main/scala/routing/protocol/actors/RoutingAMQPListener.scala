package routing.protocol.actors

import akka.actor.{ActorRef, Actor, ActorLogging, Props}
import com.github.sstone.amqp.Amqp.{Ack, Delivery}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods._
import routing.protocol.actors.RoutingAMQPListener.HandleResponse
import routing.protocol.amqp.AMQPListener
import routing.protocol.domain.form.CheckRequestForm

/**
  * Created by Rustem on 15-01-2016.
  */
object RoutingAMQPListener {

  case class HandleResponse(delivery: Delivery)

  case class RabbitRequest(delivery: Delivery)

  def props(responsePublisher: ActorRef) : Props = Props(classOf[RoutingAMQPListener], responsePublisher)

}

class RoutingAMQPListener(responsePublisher: ActorRef) extends Actor with ActorLogging with AMQPListener {
  implicit val formats = DefaultFormats
  def receive = {

    case d @ Delivery(consumerTag, envelope, properties, body) => {

      if (log.isDebugEnabled) {
        log.debug(s"--> RoutingAMQPListener Got request: ${new String(d.body)}")
        log.debug("--> d: " + d)
      }

      //correlationId (name of an actor) must be passed in the reequest
      val actorName = d.properties.getCorrelationId
      if (log.isDebugEnabled)
        log.debug("--> RoutingAMQPLIstener actorName "+actorName)
      sender() ! Ack(d.envelope.getDeliveryTag)

      val stringBody = new String(d.body)

      //req or resp
      val jsonType = (parse(stringBody) \ "jsonClass").values.toString

      log.debug("--> json resquest type: "+jsonType)

      jsonType match {
        case "Request" =>
//          val protocolActor = context.actorOf(Props(classOf[ProtocolActor]), "protocol")
          val protocolActor = context.actorOf(Props(classOf[ProtocolActor], responsePublisher))

//          val stringD = new String(d.body)
          if (log.isDebugEnabled)
            log.debug("--> stringD "+stringBody)

          val  body = (parse(stringBody) \ "body")

          val replyTo = (parse(stringBody) \ "replyTo").values.toString
          if (log.isDebugEnabled) {
            log.debug("--> body: " + compact(render(body)))
            log.debug("--> replyTo: " + replyTo)
          }
          val requestInfo = body.extract[CheckRequestForm]

          if (log.isDebugEnabled) {
            log.debug("--> requestInfo: " + requestInfo)
          }

          val checkRequest = CheckRequestProtocol(
              d.properties.getHeaders.get("stan").toString,
              self,
              requestInfo, true, replyTo)
          protocolActor ! checkRequest

        case "Response" =>
          if(actorName != null) {
            val pingActor = context.actorSelection(actorName)
            log.debug("--> found the actor ")
            pingActor ! HandleResponse(d)
          } else {
            log.error("ERROR: correlation ID is null")
          }
      }



    }

  }

}
