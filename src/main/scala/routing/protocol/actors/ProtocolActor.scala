package routing.protocol.actors

import akka.actor.{Actor, ActorRef}
import common.exception._
import org.slf4j.LoggerFactory
import routing.protocol.amqp.RequestPublisherActor
import routing.protocol.api.CustomJsonSupport
import routing.protocol.dao.{ItemDao, MerchantProtocolDao, ProductDao}
import routing.protocol.domain.exception.RoutingErrorCodes
import routing.protocol.domain.form.{CheckRequestForm, ConfirmRequestForm}
import routing.protocol.services.ProtocolService
import routing.protocol.utils.HttpPipeline
import spray.http.{HttpCharsets, HttpResponse, StatusCodes}
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

/**
 * Created by madi
 */

//case class CheckRequestProtocol(stan: String, completeActor: ActorRef, requestInfo: CheckRequestForm)

case class CheckRequestProtocol(stan: String,
                                completeActor: ActorRef,
                                requestInfo: CheckRequestForm,
                                rmq_message: Boolean,
                                responseExchange: String)



//case class ConfirmRequestProtocol(stan: String, completeActor: ActorRef, requestInfo: ConfirmRequestForm)
case class ConfirmRequestProtocol(stan: String,
                                  completeActor: ActorRef,
                                  requestInfo: ConfirmRequestForm,
                                  rmq_message: Boolean,
                                  responseExchange: String)

//class ProtocolActor(itemDao: ItemDao, protocolDao: MerchantProtocolDao, protocolService: ProtocolService) extends Actor with CustomJsonSupport{
class ProtocolActor(responsePublisher: ActorRef) extends Actor with CustomJsonSupport{

  val logger = LoggerFactory.getLogger(this.getClass)
  val itemDao = new ItemDao
  val protocolDao = new MerchantProtocolDao
  val productDao = new ProductDao
  val httpPipeline = new HttpPipeline()
  val protocolService = new ProtocolService(productDao, httpPipeline)

  logger.debug("--> ProtocolActor created: "+self.path.name.toString)
  logger.debug("--> ProtocolActor created: "+self.path.toString)

  override def receive(): Receive = {

    case CheckRequestProtocol(stan, completeActor, requestInfo, rmq_message, responseKey) => {

      logger.debug(s"--> stan=$stan: Received message to check ${requestInfo}}")

      requestInfo.productId foreach { productId =>
        val itemQueryFuture = itemDao.getItemById(productId, requestInfo.regionValue)
        itemQueryFuture map { itemDetails =>
          logger.debug(s"--> stan=$stan: Got from couch itemDetails ${itemDetails}")
          itemDetails match {
            case Some(itemDetails) => {
              val merchantQueryFuture = protocolDao.getMerchantById(itemDetails.merchantId)
              merchantQueryFuture map {
                case Some(merchantDetails) => {
                  logger.debug(s"--> stan=$stan: Got from couch merchantDetails ${merchantDetails}")
                  val futureResponse = protocolService.request(itemDetails, merchantDetails, requestInfo, Some(stan))
                  completeRequest(completeActor, futureResponse, stan, rmq_message, responsePublisher, responseKey)
                }
                case None => {
                  logger.error(s"--> stan=$stan: Merchant not found")
                  val message = "Merchant not found"
                  completeActor ! BadRequestException(RoutingErrorCodes.MERCHANT_NOT_FOUND, Some(message))
                }
              }
            }
            case None => {
              logger.error(s"--> stan=$stan: Product not found")
              val message = "Product not found"
              completeActor ! BadRequestException(RoutingErrorCodes.PRODUCT_NOT_FOUND, Some(message))
            }
          }
        }
      }
    }

    case ConfirmRequestProtocol(stan, completeActor, requestInfo, rmq_message, responseKey) => {

      logger.debug(s"--> stan=$stan: Received message to confirm $requestInfo")

      requestInfo.productId foreach { productId =>
        val itemQueryFuture = itemDao.getItemById(productId, requestInfo.regionValue)
        itemQueryFuture map {
          case Some(itemDetails) => {
            val merchantQueryFuture = protocolDao.getMerchantById(itemDetails.merchantId)
            merchantQueryFuture map {
              case Some(merchantDetails) => {
                logger.debug(s"stan=$stan Got from couch merchantDetails ${merchantDetails}")
                val futureResponse = protocolService.request(itemDetails, merchantDetails, requestInfo, Some(stan))
                completeRequest(completeActor, futureResponse, stan, rmq_message, responsePublisher,responseKey)
              }
              case None => {
                logger.error(s"--> stan=$stan: Merchant not found")
                val message = "Merchant not found"
                completeActor ! BadRequestException(RoutingErrorCodes.MERCHANT_NOT_FOUND, Some(message))
              }
            }
          }
          case None => {
            logger.error(s"--> stan=$stan: Product not found")
            val message = "Product not found (Confirm)"
            completeActor ! BadRequestException(RoutingErrorCodes.PRODUCT_NOT_FOUND, Some(message))
          }
        }
      }
    }
  }


  //TODO: do we need to become receive after this>
  def waitingPublisherResponse(/*onSuccessBehaviour: Receive = waitingResponse()*/): Receive = {

    case RequestPublisherActor.Success(_) =>

      logger.debug(s"--> message published successfully.")

      context.stop(self)
//      context.become(receive)

    case RequestPublisherActor.Failure(_, ex) =>
      logger.error(s"--> Failed to publish message to Rabbit MQ. Exception: ${ex.toString()}.")
//      context.parent ! ServerErrorRequestException(DarErrorCodes.RABBITMQ_PUBLISH_ERROR(DarErrorSeries.PROFILE_API))
      context.become(receive)

  }
  private def completeRequest(completeActor: ActorRef,
                              future: Future[HttpResponse],
                              stan: String,
                              rmq_message: Boolean,
                              requestPublisher: ActorRef,
                              responseKey: String): Unit = {

//    println("sender: "+sender())
    if (logger.isDebugEnabled) {
      logger.debug("--> rmq_message: " + rmq_message)
      logger.debug("--> requestPublisher: " + requestPublisher.toString())
    }

    future onSuccess {
      case response => {

        if (logger.isDebugEnabled)
          logger.debug(s"--> stan=$stan: Merchant endpoint status ${response.status.intValue}. Body: ${response.entity.asString}")

        val body = response.entity.asString(HttpCharsets.`UTF-8`)


        if (logger.isDebugEnabled)
          logger.debug("--> body: "+body)

        response.status match {

          case StatusCodes.OK | StatusCodes.Created | StatusCodes.Accepted | StatusCodes.NoContent | StatusCodes.AlreadyReported =>
            logger.debug(s"--> stan=$stan: Received status OK")
            logger.debug("--> response: "+response)

            if (rmq_message){
              val message = CheckResponse("jwt","stan",body, responseKey)
              logger.debug("--> Protocol Actor message: "+message)
              val actorName = self.path.toStringWithoutAddress
              logger.debug("--> RoutingActor actorName: "+actorName)
              responsePublisher ! RequestPublisherActor.PublishToQueue(actorName, message)
              context.become(waitingPublisherResponse())
            }
            else
              completeActor ! response
          case _ =>
            val errorInfo = Try{body.parseJson.convertTo[ErrorInfo]}.toOption

            errorInfo match {

              case Some(info) =>
                info.status match {

                  case Some(StatusCodes.Unauthorized.intValue) =>
                    logger.error(s"stan=$stan: Received Unauthorized StatusCode ${StatusCodes.Unauthorized.intValue}")
                    completeActor ! (UnauthorizedErrorException(RoutingErrorCodes.CustomErrorCode(info.series.get, info.code.get.toInt % 1000), info.developerMessage))

                  case Some(StatusCodes.BadRequest.intValue) =>
                    logger.error(s"stan=$stan: Received BadRequest StatusCode ${StatusCodes.BadRequest.intValue}")
                    completeActor ! (BadRequestException(RoutingErrorCodes.CustomErrorCode(info.series.get, info.code.get.toInt % 1000), info.developerMessage))

                  case Some(StatusCodes.Forbidden.intValue) =>
                    logger.error(s"stan=$stan: Received Forbidden StatusCode ${StatusCodes.Forbidden.intValue}")
                    completeActor ! (ForbiddenErrorException(RoutingErrorCodes.CustomErrorCode(info.series.get, info.code.get.toInt % 1000), info.developerMessage))

                  case Some(StatusCodes.NotFound.intValue) =>
                    logger.error(s"stan=$stan: Received NotFound StatusCode ${StatusCodes.NotFound.intValue}")
                    completeActor ! (NotFoundException(RoutingErrorCodes.CustomErrorCode(info.series.get, info.code.get.toInt % 1000), info.developerMessage))

                  case Some(StatusCodes.InternalServerError.intValue) =>
                    logger.error(s"stan=$stan: Received InternalServerError StatusCode. Sending ServerErrorRequest")
                    completeActor ! (ServerErrorRequestException(RoutingErrorCodes.CustomErrorCode(info.series.get, info.code.get.toInt % 1000), info.developerMessage))

                  case _ =>
                    logger.error(s"stan=$stan: Received None. Sending ServerErrorRequest")
                    completeActor ! (ServerErrorRequestException(RoutingErrorCodes.CustomErrorCode(info.series.get, info.code.get.toInt % 1000), info.developerMessage))
                }

              case None =>
                logger.error(s"stan=$stan: ErrorInfo empty. Unknown exception, sending ServerErrorRequest")
                completeActor ! (ServerErrorRequestException(RoutingErrorCodes.MERCHANT_ERROR, Some("Unknown exception")))
            }
        }
      }
    }

    future onFailure {
      case ex => {
        logger.error(s"stan=$stan: Failed to send request to merchant endpoint. Message: ${ex.getMessage}")
        val message = ex.getMessage
        completeActor ! ServerErrorRequestException(RoutingErrorCodes.MERCHANT_ERROR, Some(message))
      }
    }
  }

}
