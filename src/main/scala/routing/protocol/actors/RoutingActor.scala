package routing.protocol.actors

import akka.actor.{Actor, ActorRef, Props}
import common.exception.UnauthorizedErrorException
import kz.dar.eco.common.exception.{DarErrorCodes, DarErrorSeries, ServerErrorRequestException}
import kz.dar.eco.domain.core.Endpoint
import kz.dar.eco.domain.user.UserContext
import kz.dar.eco.messages.core.{DomainMessage, RequestMessageType}

//import kz.dar.eco.wallet.api.routing.ApiMessage
import org.slf4j.LoggerFactory
import routing.protocol.amqp.RequestPublisherActor
import routing.protocol.commands.RoutingCommands
import routing.protocol.core.ApiMessage
import routing.protocol.domain.exception.RoutingErrorCodes
import routing.protocol.domain.form.{CheckRequestForm, ConfirmRequestForm}
import routing.protocol.services.JwtService

/**
 * Created by darik on 9/3/15.
 */


//case class CheckRequest(jwt: String, stan: String, requestInfo: CheckRequestForm) extends RequestMessage
case class CheckRequest(jwt: String, stan: String, requestInfo: CheckRequestForm) extends ApiMessage[CheckRequestForm]{
  override val headers: Map[String, String] = Map[String, String]()
  override val messageType: RequestMessageType = RoutingCommands.CheckReq
  override val userContext: Option[UserContext] = None
  override val body: CheckRequestForm = requestInfo
}

//for RMQ
case class CheckResponse(jwt: String, stan: String, response: String, respKey: String) extends ApiMessage[String]{
  override val headers: Map[String, String] = Map[String, String]()
  override val messageType: RequestMessageType = RoutingCommands.CheckReq
  override val userContext: Option[UserContext] = None
  override val body: String = response
  val responseKey: String = respKey
}

case class CheckRequestRMQ(jwt: String, stan: String, requestInfo: CheckRequestForm) extends ApiMessage[CheckRequestForm]{
//  override val headers: Map[String, String] = Map[String, String]()
  override val headers: Map[String, String] = Map("jwt"->jwt, "stan"->stan)
  override val messageType: RequestMessageType = RoutingCommands.CheckReqRMQ
  override val userContext: Option[UserContext] = None
  override val body: CheckRequestForm = requestInfo
}
//case class ConfirmRequest(jwt: String, stan: String, requestInfo: ConfirmRequestForm) extends RequestMessage
case class ConfirmRequest(jwt: String, stan: String, requestInfo: ConfirmRequestForm) extends ApiMessage[ConfirmRequestForm]{
  override val headers: Map[String, String] = Map[String, String]()
  override val messageType: RequestMessageType = RoutingCommands.ConfirmReq
  override val userContext: Option[UserContext] = None
  override val body: ConfirmRequestForm = requestInfo
}

class RequestMessage()

object RoutingActor {
//  case class CheckRequestRMQ(jwt: String, stan: String, requestInfo: CheckRequestForm) extends ApiMessage[CheckRequestForm]  {
//      override val headers: Map[String, String] = Map[String, String]()
//    override val messageType = RoutingCommands.CheckReq
//      override val userContext: Option[UserContext] = None
//    override val body = requestInfo
//  }

    def props(jwtService: JwtService, requestPublisher: ActorRef)
             (implicit endpoint: Endpoint): Props =
      Props(new RoutingActor(jwtService, requestPublisher))
}
class RoutingActor(jwtService: JwtService, requestPublisher: ActorRef)
                  (implicit endpoint: Endpoint) extends Actor{
  def replyTo(apiMessage: ApiMessage[_]): String = {
    s"${endpoint.instanceEndpoint}.response.${apiMessage.messageType.name}"
//    s"routing.response.queue.or.whatever"
  }
  val logger = LoggerFactory.getLogger(this.getClass)
//  val protocolActor = context.actorSelection("/user/application/protocol")
  val protocolActor = context.actorOf(Props(classOf[ProtocolActor], requestPublisher), "protocol")

  if (logger.isDebugEnabled) {
    logger.debug("--> RoutingActor created: " + self.path.name.toString)
    logger.debug("--> RoutingActor created: " + self.path.toString)
    logger.debug("--> RoutingActor created: " + self.toString)
  }
  override def receive: Receive = {

    case CheckRequest(jwt, stan, requestInfo) => {

      logger.debug(s"--> Received message to check request jwt=${ jwt}, stan=${stan}, data=${requestInfo}")

//      println("sender: "+sender())
      val validJwt = jwtService.decodeToken(jwt)
      validJwt match {
        case Some(jwt) =>
          protocolActor ! CheckRequestProtocol(stan, sender(), requestInfo, false,"")
//          context.become(waitingResponse())
        case None => {
          val message = "Invalid authorization token provided"
          sender() ! Left(UnauthorizedErrorException(RoutingErrorCodes.AUTHORIZATION_ERROR, Some(message)))
        }
      }
    }
    case ConfirmRequest(jwt, stan, requestInfo) => {

      logger.debug(s"--> Received message to confirm request jwt=${jwt}, stan=${stan}, data=${requestInfo.addData}")

      val validJwt = jwtService.decodeToken(jwt)
      validJwt match {
        case Some(jwt) => protocolActor ! ConfirmRequestProtocol(stan, sender(), requestInfo, false, "")
        case None => {
          val message = "Invalid authorization token provided"
          sender() ! Left(UnauthorizedErrorException(RoutingErrorCodes.AUTHORIZATION_ERROR, Some(message)))
        }
      }
    }
    //    case _ =>
    case m @ CheckRequestRMQ(jwt, stan, requestInfo) => {
      logger.debug("Routing actor CheckRequestRMQ case: "+m)
      //
      //      logger.debug(s"Received message to check request jwt=${ jwt}, stan=${stan}, data=${requestInfo.addData}")
      val message = DomainMessage.request[CheckRequestForm](m.messageType, Some(replyTo(m)), m.headers, m.userContext, m.body)
      logger.debug("message: "+message)
      val actorName = self.path.toStringWithoutAddress
      logger.debug("RoutingActor actorName: "+actorName)
      requestPublisher ! RequestPublisherActor.PublishToQueue(actorName, message)
      context.become(waitingPublisherResponse())
      //      val validJwt = jwtService.decodeToken(jwt)
      //      validJwt match {
      //        case Some(jwt) => protocolActor ! CheckRequestProtocol(stan, sender(), requestInfo)
      //        case None => {
      //          val message = "Invalid authorization token provided"
      //          sender() ! Left(UnauthorizedErrorException(RoutingErrorCodes.AUTHORIZATION_ERROR, Some(message)))
      //        }
      //      }
    }
    case _ =>
      logger.debug("--> RoutingActor default case")
  }
  def waitingPublisherResponse(/*onSuccessBehaviour: Receive = waitingResponse()*/): Receive = {

    case RequestPublisherActor.Success(_) =>

      logger.debug(s"message published successfully.")

      context.become(receive)

    case RequestPublisherActor.Failure(_, ex) =>
      logger.error(s"Failed to publish message to Rabbit MQ. Exception: ${ex.toString()}.")
      context.parent ! ServerErrorRequestException(DarErrorCodes.RABBITMQ_PUBLISH_ERROR(DarErrorSeries.PROFILE_API))
      context.become(receive)

  }
}
