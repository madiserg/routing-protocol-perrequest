package routing.protocol.api

import common.exception.ErrorInfo
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import routing.protocol.domain.form._
import routing.protocol.utils.JWTObject
import spray.httpx.SprayJsonSupport
import spray.json._

/**
 * JSON formatters
 *
 * Created by Madi
 */
trait CustomJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit object DateJsonFormat extends RootJsonFormat[DateTime] {
    private val parserISO: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

    override def write(obj: DateTime) = JsString(parserISO.print(obj))

    override def read(json: JsValue): DateTime = json match {
      case JsString(s) => parserISO.parseDateTime(s)
      case _ => throw new DeserializationException("DateJsonFormat: Object expected")
    }
  }

  implicit val UserInfoFormat = jsonFormat6(UserInfo)

  implicit val PaymentAmountFormat = jsonFormat2(PaymentAmount)

  implicit val IdentifierFormat = jsonFormat3(Identifier)

  implicit val UserFormat = jsonFormat12(User)

  implicit val RegionValueFormat = jsonFormat2(RegionValue)

  implicit val CheckRequestFormFormat = jsonFormat13(CheckRequestForm)

  implicit val ConfirmRequestFormFormat = jsonFormat14(ConfirmRequestForm)

  implicit val TransferCheckFormFormat = jsonFormat13(TransferCheckForm)

  implicit val TransferConfirmFormFormat = jsonFormat14(TransferConfirmForm)

  implicit val jwtObjectFormat = jsonFormat1(JWTObject)

  implicit val errorInfoFormat = jsonFormat6(ErrorInfo)
}