package routing.protocol.api

import java.util.Locale

import akka.actor.{Actor, ActorRef, ActorSystem}
import com.typesafe.config.ConfigFactory
import kz.dar.eco.domain.core.Endpoint
import kz.dar.eco.messages.core.DomainMessage
import org.json4s.DefaultFormats
import org.slf4j.LoggerFactory
import routing.protocol.actors.RoutingAMQPListener.RabbitRequest
import routing.protocol.actors.{CheckRequest, CheckRequestRMQ, ConfirmRequest, RoutingActor}
import routing.protocol.core.{CustomErrorHandler, DefaultTimeout, Marshalling, PerRequestCreator}
import routing.protocol.domain.exception.{LocalizedMessages, RoutingErrorCodes, RoutingErrorSeries}
import routing.protocol.domain.form.{CheckRequestForm, ConfirmRequestForm}
import routing.protocol.services.JwtService
import spray.http.{ContentTypes, HttpEntity, StatusCodes}
import spray.routing._
import spray.util.LoggingContext
import org.json4s.native.JsonMethods._
import scala.concurrent.ExecutionContext

/**
 * Created by Madi
 */

//class RoutingProtocolAPI(routingActor: ActorRef)(implicit val actorSystem: ActorSystem) extends Directives with DefaultTimeout with CustomJsonSupport with Marshalling {
//class RoutingProtocolAPI()(implicit val actorSystem: ActorSystem) extends HttpServiceActor with CustomErrorHandler with Directives with HttpService with PerRequestCreator with Actor with DefaultTimeout with CustomJsonSupport with Marshalling {
class RoutingProtocolAPI(requestPublisher: ActorRef)
                        (implicit val actorSystem: ActorSystem,
                         ecoRoutingService: Endpoint) extends HttpServiceActor with CustomErrorHandler with Directives with HttpService with PerRequestCreator with Actor with DefaultTimeout with CustomJsonSupport with Marshalling {
  val logger = LoggerFactory.getLogger(this.getClass)
  def getRejectionMessage(message: String, language: String) : String = {
    logger.debug("Rejection error")
    val userMessage = LocalizedMessages.getMessage(new Locale(language), message.substring(20))
    val code = "400"+RoutingErrorCodes.INPUT_PARAM_VALIDATION_ERROR.series.series+RoutingErrorCodes.INPUT_PARAM_VALIDATION_ERROR.code
    s"""{"series":${RoutingErrorSeries.REQUEST_ERROR.series}, "code":${code}, "moreInfo":"docs/${RoutingErrorCodes.INPUT_PARAM_VALIDATION_ERROR}", "status":400, "developerMessage": "${message}", "message":"${userMessage}"}"""
  }

  implicit val customRejectionHandler = RejectionHandler {

    case ValidationRejection(message, _) :: _ =>
      complete(StatusCodes.BadRequest, HttpEntity(ContentTypes.`application/json`, s"""{"developerMessage": "${message}", "message":"${message.substring(20)}"}"""))

    case MalformedRequestContentRejection(message, _) :: _ =>
      complete(StatusCodes.BadRequest, HttpEntity(ContentTypes.`application/json`, s"""{"developerMessage": "${message}", "message":"${message.substring(20)}"}"""))
  }
  implicit val formats = DefaultFormats
  implicit val executionContext: ExecutionContext = context.dispatcher

  override implicit def actorRefFactory = context

  logger.debug("roting protocol api actor name "+self.path.toStringWithoutAddress)
//  println("roting protocol api actor name "+self.path.toStringWithAddress)

  val LOGGER = LoggerFactory.getLogger(this.getClass)
//  implicit val ctx: RequestContext
  def receive = route2 orElse runRoute(route)(customExceptionHandler, customRejectionHandler, actorRefFactory,
      RoutingSettings.default(actorRefFactory), LoggingContext.fromActorContext(actorRefFactory))

  val route = pathPrefix("api") {
    post {
//      println("post")
      path("check") {
        entity(as[CheckRequestForm]) { (checkRequestInfo) =>
          headerValueByName("Authorization") { jwt =>
            headerValueByName("stan") { stan =>
//              complete {
////                HttpResponse(entity = "foo")
////                (routingActor ? CheckRequest(jwt, stan, checkRequestInfo)).mapTo[Either[ApiException, String]]
//              }
              LOGGER.debug("--> received check request")
              handleRequest{
                CheckRequest(jwt, stan, checkRequestInfo)
              }
            }
          }
        }
      }~
      path("confirm") {
        entity(as[ConfirmRequestForm]) { confirmRequestInfo =>
          headerValueByName("Authorization") { jwt =>
            headerValueByName("stan") { stan =>
//              complete {
//                (routingActor ? ConfirmRequest(jwt, stan, confirmRequestInfo)).mapTo[Either[ApiException, String]]
//              }
              LOGGER.debug("--> received confirm request")
              handleRequest{
                ConfirmRequest(jwt, stan, confirmRequestInfo)
              }
            }
          }
        }
      }~
      path("checkrmq") {
        entity(as[CheckRequestForm]) { (checkRequestInfo) =>
          headerValueByName("Authorization") { jwt =>
            headerValueByName("stan") { stan =>
              //              complete {
              ////                HttpResponse(entity = "foo")
              ////                (routingActor ? CheckRequest(jwt, stan, checkRequestInfo)).mapTo[Either[ApiException, String]]
              //              }
              LOGGER.debug("received rmq request")
              handleRequest{
                CheckRequestRMQ(jwt, stan, checkRequestInfo)
              }
            }
          }
        }
      }
    }
  }

  val route2: Receive = {
    case RabbitRequest(d) =>
      logger.debug("routing protocol RabbitRequest case")
//      println("d: "+d.toString)
      val stringD = new String(d.body)
      logger.debug("stringD "+stringD)
      val  body = (parse(stringD) \ "body")

      logger.debug("body: "+compact(render(body)))
      val requestInfo = body.extract[CheckRequestForm]
      logger.debug("requestInfo: "+requestInfo)
      handleRequestRMQ{
        CheckRequest(
          d.properties.getHeaders.get("jwt").toString,
          d.properties.getHeaders.get("stan").toString,
          requestInfo
        )
      }
  }

  val config = ConfigFactory.load()
  val checkToken = config.getBoolean("prod")
  val jwtService = new JwtService(checkToken)

//  def handleRequest(message : RequestMessage): Route = {
//    ctx => perRequest(ctx, RoutingActor.props(jwtService, requestPublisher), message)
//  }
  def handleRequest(message : DomainMessage[_]): Route = {

    logger.debug("handleRequest: "+message)

      ctx =>
        logger.debug("ctx "+ctx)
        perRequest(ctx, RoutingActor.props(jwtService, requestPublisher), message)

}
  def handleRequestRMQ(message : DomainMessage[_])
//                      (implicit  ctx: RequestContext)
  : Route = {
    logger.debug("handleRequest: "+message)
    logger.debug("handleRequest: "+actorRefFactory)
    ctx => perRequest(ctx, RoutingActor.props(jwtService, requestPublisher), message)
  }

}

