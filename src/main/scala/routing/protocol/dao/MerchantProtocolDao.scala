package routing.protocol.dao

import com.ibm.couchdb.Res.DocOk
import com.ibm.couchdb.TypeMapping
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.concurrent.{Future, Promise}
import scalaz.{-\/, \/-}

/**
 * Created by Madi
 */

case class BankData(IBAN: String)
case class EpsData(eAccount: String)
case class Security(hashAlgorithm: String, accessToken: String)
case class Protocol(checkUrl: String, confirmUrl: String, format: String, security: Security)
case class Endpoint(checkUrl: String, confirmUrl: String)
case class MerchantDetails(merchantId: Long, name: String, protocol: Protocol, endpoint: Option[Endpoint], bankData: BankData, epsData: EpsData)


class MerchantProtocolDao extends CouchDBConnection {

  val logger = LoggerFactory.getLogger(this.getClass)

  val dbName = ConfigFactory.load().getString("couch_db.database")
  val typeMapping = TypeMapping(classOf[MerchantDetails] -> "MerchantDetails")

  val db = couch.db(dbName, typeMapping)

  def getMerchantById(merchantId: Long): Future[Option[MerchantDetails]] = {
    val merchantQuery = db.query.view[Long, MerchantDetails]("merchantId-design", "merchantId-view")
                          .get.key(merchantId)

    val action = merchantQuery.query
    val promise = Promise[Option[MerchantDetails]]()

    action.runAsync {
      case -\/(e) => {
        logger.debug(s"Exception: ${e.getMessage()}")
        promise.failure(e)
      }
      case \/-(doc) => {
        logger.debug(s"Merchant details have been successfully retrieved")
        if (doc.rows.length > 0) {
          promise.success(Some(doc.rows(0).value))
        }else {
          promise.success(None)
        }
      }
    }

    promise.future
  }

  def addMerchant(merchantDetails: MerchantDetails): Future[DocOk] = {
    val action = db.docs.create(merchantDetails)
    val promise = Promise[DocOk]

    action.runAsync {
      case -\/(e) => {
        promise.failure(e)
      }
      case \/-(docOk) => {
        promise.success(docOk)
      }
    }
    promise.future
  }
}
