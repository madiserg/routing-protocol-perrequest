package routing.protocol.dao

import com.ibm.couchdb.TypeMapping
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory
import routing.protocol.domain.form.RegionValue

import scala.concurrent.{Future, Promise}
import scalaz.{-\/, \/-}

/**
 * Created by Madi
 */

case class Money(amount: Double,
                 currency: String)

case class Vendor(id: Long,
                  name: String)

case class CommissionFormula(min: Double,
                             max: Double,
                             percentage: Double,
                             fixed: Double)

case class ProductMap(merchantId: Long,
                      productId: Long,
                      partnerCode: String,
                      partnerPrice: Money,
                      vendor: Vendor,
                      commissionFormula: CommissionFormula,
                      regionValue: Option[RegionValue],
                      keys: Seq[String],
                      values: Seq[String])

class ItemDao extends CouchDBConnection{
  val logger = LoggerFactory.getLogger(this.getClass)

  val dbName = ConfigFactory.load().getString("couch_db.database")
  val typeMapping = TypeMapping(classOf[ProductMap] -> "ProductMap")

  val db = couch.db(dbName, typeMapping)

  def getItemById(itemId: Long): Future[Option[ProductMap]] = {
    val itemQuery = db.query.view[Long, ProductMap]("item-details-design", "item-details-view")
      .get.key(itemId)

    val action = itemQuery.queryIncludeDocs[ProductMap]
    val promise = Promise[Option[ProductMap]]()

    action.runAsync {
      case -\/(e) => {
        logger.debug(s"Failed while retrieving data. Exception message: ${e.getMessage()}")
        promise.failure(e)
      }
      case \/-(doc) => {
        logger.debug(s"Item details have been successfully retrieved ${doc.getDocsData.headOption}")
        promise.success(doc.getDocsData.headOption)
      }
    }

    promise.future
  }

  def getItemById(itemId: Long, regionValue: Option[RegionValue]): Future[Option[ProductMap]] = {
    regionValue match {
      case None => getItemById(itemId)
      case Some(region) => {
        val itemQuery = db.query.view[(Long, String, String), ProductMap]("item-details-byRegionCity", "item-details-byRegionCity")
          .get.key(itemId, region.country, region.city)

        val action = itemQuery.queryIncludeDocs[ProductMap]
        val promise = Promise[Option[ProductMap]]

        action.runAsync {
          case -\/(e) => promise.failure(e)
          case \/-(doc) => promise.success(doc.getDocsData.headOption)
        }
        promise.future
      }
    }
  }
}
