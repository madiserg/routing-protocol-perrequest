package routing.protocol.dao

import com.ibm.couchdb.TypeMapping
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.concurrent.{Future, Promise}
import scalaz.{-\/, \/-}

/**
 * Created by Madi
 */

case class ItemDetails(id: Int, // just for back compatibility
                       order: Int,
                       name: String,
                       title: String,
                       defaultValue: String,
                       baseHint: String,
                       extraHint: String,
                       errorMessage: String,
                       regex: String,
                       hidden: Boolean,
                       required: Boolean)

case class City(code: String,
                name: String,
                name_kk: String,
                name_en: String,
                name_ru: String)

case class RegionMeta(code: String,
                      name: String,
                      name_kk: String,
                      name_en: String,
                      name_ru: String,
                      cities: Seq[City])

case class Item(id: Long,
                productCode: String,
                categoryId: Long,
                regionMeta: Seq[RegionMeta],
                price: Money,
                url: String,
                idx: Int,
                details: Seq[ItemDetails],
                logo: String)

class ProductDao extends CouchDBConnection {
  val logger = LoggerFactory.getLogger(this.getClass)

  val dbName = ConfigFactory.load().getString("couch_db.database")
  val typeMapping = TypeMapping(classOf[ProductMap] -> "Item")

  val db = couch.db(dbName, typeMapping)

  def getProductById(productId: Long): Future[Option[Item]] = {
    val productQuery = db.query.view[Long, Item]("byProductId", "byProductId")
      .get.key(productId)

    val action = productQuery.queryIncludeDocs[Item]

    val promise = Promise[Option[Item]]
    action.runAsync {
      case -\/(e) => promise.failure(e)
      case \/-(doc) => promise.success(doc.getDocsData.headOption)
    }

    promise.future
  }
}
