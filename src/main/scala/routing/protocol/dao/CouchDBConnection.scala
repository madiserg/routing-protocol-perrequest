package routing.protocol.dao

import com.ibm.couchdb.CouchDb
import com.typesafe.config.ConfigFactory

/**
 * Created by Madi
 */
trait CouchDBConnection {
  val config = ConfigFactory.load()
  val couch = CouchDb(config.getString("couch_db.hostname"), config.getInt("couch_db.port"))
}
