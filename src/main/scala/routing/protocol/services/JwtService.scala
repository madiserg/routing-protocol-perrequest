package routing.protocol.services

import authentikat.jwt._

/**
 * Created by Madi
 */

class JwtService(checkToken: Boolean) {

  private lazy val internalSecret = "k38HeibGciOiJIbWFjU0hBMjU2IiwidHlwIjoiSldUIn0eyJ1c2"

  def generateJwt(data: Map[String, String], secret: String): String = {
    JsonWebToken(JwtHeader("HS256"), JwtClaimsSet(data), secret)
  }

  def decodeToken(token: String): Option[Map[String, String]] = {

    checkToken match {
      case false => Some(Map[String, String]())
      case true => {
        if (JsonWebToken.validate(token, internalSecret)) {
          val claims: Option[Map[String, String]] = token match {
            case JsonWebToken(header, claimsSet, signature) =>
              claimsSet.asSimpleMap.toOption
            case x =>
              None
          }
          claims
        }
        else None
      }
    }
  }
}