package routing.protocol.services

import authentikat.jwt.{JsonWebToken, JwtClaimsSet, JwtHeader}
import routing.protocol.dao.{MerchantDetails, ProductDao, ProductMap}
import routing.protocol.domain.form._
import routing.protocol.utils.HttpPipeline
import spray.http.HttpResponse

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by darik on 9/3/15.
 */
class ProtocolService(productDao: ProductDao, httpPipeline: HttpPipeline) {

  def list2map(keys: Seq[String], values: Seq[String]): mutable.Map[String, String] = {
    collection.mutable.Map(keys.zip(values).toArray: _*)
  }

  def getJwt(data: Map[String, String], merchantDetails: MerchantDetails): String = {
    val header = JwtHeader("HS256")
    val claimsSet = JwtClaimsSet(data)
    JsonWebToken(header, claimsSet, merchantDetails.protocol.security.accessToken)
  }

  def getUserInfoMap(userInfo: UserInfo): Map[String, String] = {
    val map = mutable.HashMap[String, String]()

    map("user_id") = userInfo.id.toString
    userInfo.ip match {
      case Some(ip) => map("ip") = ip
      case None => map("ip") = "127.0.0.1"
    }
    userInfo.email foreach { email =>
      map("email") = email
    }
    userInfo.username foreach { username =>
      map("username") = username
    }

    map.toMap
  }

  def addMerchantDetails(map: Option[Map[String, String]], merchantDetails: MerchantDetails): Map[String, String] = {
    val data = mutable.Map[String, String]()
    map foreach { map =>
      for((k, v) <- map) data(k) = v
    }
    merchantDetails.endpoint match {
      case Some(endpoint) => {
        data("endpoint.checkUrl") = endpoint.checkUrl
        data("endpoint.confirmUrl") = endpoint.confirmUrl
      }
      case None => {
        data("endpoint.checkUrl") = merchantDetails.protocol.checkUrl
        data("endpoint.confirmUrl") = merchantDetails.protocol.confirmUrl
      }
    }
    data("endpoint.jwtSecret") = merchantDetails.protocol.security.accessToken
    data("merchantId") = merchantDetails.merchantId.toString

    data.toMap
  }

  def request(url: String, data: Map[String, String], jwt: String, stan: Option[String]): Future[HttpResponse] = {
    httpPipeline.post(url, jwt, data, stan)
  }

  def request(url: String, data: Map[String, String], stan: Option[String]): Future[HttpResponse] = {
    httpPipeline.post(url, data, stan)
  }

  def request(itemDetails: ProductMap, merchantDetails: MerchantDetails, requestInfo: CheckRequestForm, stan: Option[String]): Future[HttpResponse] = {
    val map = getUserInfoMap(requestInfo.userInfo)
    val jwt = getJwt(map, merchantDetails)

    productDao.getProductById(itemDetails.productId) flatMap {
      case Some(productDetails) => {
        var keys = ArrayBuffer[String]()
        requestInfo.keys match {
          case None => {
            for(details <- productDetails.details) keys += details.name
          }
          case Some(details) => {
            if (details.length == requestInfo.values.length) {
              for(detail <- details) keys += detail
            }else {
              for(details <- productDetails.details) keys += details.name
            }
          }
        }

        val transferFrom = TransferCheckForm(
          itemDetails.productId,
          itemDetails.partnerCode,
          requestInfo.userInfo,
          requestInfo.paymentAmount,
          addMerchantDetails(requestInfo.addData, merchantDetails),
          requestInfo.srcUser,
          requestInfo.destUser,
          requestInfo.operationCreated,
          requestInfo.operationDate,
          requestInfo.operationId,
          requestInfo.regionValue,
          keys.toSeq,
          requestInfo.values
        )

        httpPipeline.post(merchantDetails.protocol.checkUrl, jwt, transferFrom, stan)
      }
      case None => Future.failed(new Exception("Product not found"))
    }
  }

  def request(itemDetails: ProductMap, merchantDetails: MerchantDetails, requestInfo: ConfirmRequestForm, stan: Option[String]): Future[HttpResponse] = {
    val map = getUserInfoMap(requestInfo.userInfo)
    val jwt = getJwt(map, merchantDetails)

    productDao.getProductById(itemDetails.productId) flatMap {
      case Some(productDetails) => {
        var keys = ArrayBuffer[String]()
        requestInfo.keys match {
          case None => {
            for(details <- productDetails.details) keys += details.name
          }
          case Some(details) => {
            if (details.length == requestInfo.values.length) {
              for(detail <- details) keys += detail
            }else {
              for(details <- productDetails.details) keys += details.name
            }
          }
        }

        val transferFrom = TransferConfirmForm(
          itemDetails.productId,
          itemDetails.partnerCode,
          requestInfo.userInfo,
          requestInfo.paymentAmount,
          addMerchantDetails(requestInfo.addData, merchantDetails),
          requestInfo.status,
          requestInfo.srcUser,
          requestInfo.destUser,
          requestInfo.operationCreated,
          requestInfo.operationDate,
          requestInfo.operationId,
          requestInfo.regionValue,
          keys.toSeq,
          requestInfo.values
        )

        httpPipeline.post(merchantDetails.protocol.confirmUrl, jwt, transferFrom, stan)
      }
      case None => Future.failed(new Exception("Product not found"))
    }
  }
}
