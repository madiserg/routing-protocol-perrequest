package routing.protocol.commands

import kz.dar.eco.messages.core.RequestMessageType

/**
  * Created by madi on 9/13/16.
  */
object RoutingCommands {

  case object CheckReq extends RequestMessageType {
    override val target = Some("routing")
    override val name = "checkReq"
    override val flow = Some("GET")
    override val routingKey = key
  }
  case object CheckReqRMQ extends RequestMessageType {
    override val target = Some("routing")
    override val name = "checkReqRMQ"
    override val flow = Some("GET")
    override val routingKey = key
  }
  case object ConfirmReq extends RequestMessageType {
    override val target = Some("routing")
    override val name = "confirmReq"
    override val flow = Some("GET")
    override val routingKey = key
  }
}
