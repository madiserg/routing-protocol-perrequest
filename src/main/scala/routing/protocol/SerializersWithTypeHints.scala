package routing.protocol

import kz.dar.eco.domain.serializers._
import kz.dar.eco.messages.MessageSerializer
import org.json4s.ShortTypeHints
import org.json4s.native.Serialization

/**
  * Created by Madi on 31-08-2016
  */
trait SerializersWithTypeHints extends DateTimeSerializer with ProfileSerializer with PersonaSerializer with UserContextSerializer with MessageSerializer  with CoreSerializers {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(
    ShortTypeHints(
      coreTypeHints ++
      profileTypeHints ++
      personaTypeHints ++
      userContextTypeHints ++
      messageHintTypes
    )
  ) +
    new DateTimeSerializer() ++
    profileFormats ++
    userContextFormats ++
    personaFormats addKeySerializers
    profileKeyFormats

}
