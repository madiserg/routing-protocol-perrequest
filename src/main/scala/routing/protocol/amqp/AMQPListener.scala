package routing.protocol.amqp

import org.json4s.reflect.Reflector

/**
  * Created by Rustem on 15-01-2016.
  */
trait AMQPListener {

  protected def classFor(hint: String): Option[Class[_]] = Reflector.scalaTypeOf(hint).map(_.erasure)

}
