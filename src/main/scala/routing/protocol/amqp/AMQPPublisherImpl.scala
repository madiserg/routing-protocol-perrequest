package routing.protocol.amqp

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.Publish
import com.github.sstone.amqp.{Amqp, ChannelOwner, ConnectionOwner, RpcClient}
import com.rabbitmq.client.AMQP

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

trait AMQPPublisher {

  def publish(data            : String,
              routingKey      : String,
              exchange        : String,
              correlationId   : Option[String] = None,
              replyTo         : Option[String] = None,
              messageId       : Option[String] = None,
              `type`          : Option[String] = None,
              contentType     : String = "application/json",
              contentEncoding : String = "UTF-8",
              headers         : Option[Map[String, AnyRef]] = None) : Future[Option[RpcClient.Response]]

}

/**
  * Created by Madi on 31-08-2016
  */
class AMQPPublisherImpl(connection: AMQPConnection)(implicit system: ActorSystem) extends AMQPPublisher {

  val client = ConnectionOwner.createChildActor(connection.conn, ChannelOwner.props())

  // wait till everyone is actually connected to the broker
  Amqp.waitForConnection(system, client).await()

  /**
    * @inheritdoc
    */
  override def publish(data            : String,
              routingKey      : String,
              exchange        : String,
              correlationId   : Option[String] = None,
              replyTo         : Option[String] = None,
              messageId       : Option[String] = None,
              `type`          : Option[String] = None,
              contentType     : String = "application/json",
              contentEncoding : String = "UTF-8",
              headers         : Option[Map[String, AnyRef]] = None) : Future[Option[RpcClient.Response]] = {

    implicit val timeout: Timeout = 60 seconds

    val body = data.getBytes

    val javaHeaders: java.util.Map[String, AnyRef] = headers match {
      case Some(h) => mapAsJavaMap[String, AnyRef](h)
      case _ => null
    }

    val properties = new AMQP.BasicProperties.Builder()
      .contentType(contentType)
      .contentEncoding(contentEncoding)
      .messageId(messageId.orNull)
      .replyTo(replyTo.orNull)
      .correlationId(correlationId.orNull)
      .`type`(`type`.orNull)
      .headers(javaHeaders)
      .build()

    val res = client ? Publish(exchange, routingKey, body, Some(properties))

    res.map {
      case Amqp.Ok(request, result) => None
      case Amqp.Error(request, reason) => throw reason
    }

  }

}
