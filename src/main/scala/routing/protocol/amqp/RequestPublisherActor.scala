package routing.protocol.amqp

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.Publish
import com.github.sstone.amqp.{Amqp, ChannelOwner, ConnectionOwner}
import com.rabbitmq.client.AMQP
import kz.dar.eco.messages.core.{DomainMessage, Request}

import org.json4s.native.Serialization._
import org.slf4j.LoggerFactory
import routing.protocol.SerializersWithTypeHints
import routing.protocol.actors.CheckResponse
import routing.protocol.amqp.RequestPublisherActor.PublishToQueue

import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
  * Created by Madi on 31-08-2016
  */
object RequestPublisherActor {

  case class PublishToQueue(correlationId: String, dm: DomainMessage[_])

  case class Success(correlationId: String)

  case class Failure(correlationId: String, reason: Throwable)

  def props(amqpConnection: AMQPConnection, gate: String): Props = Props(classOf[RequestPublisherActor], amqpConnection, gate)

}

class RequestPublisherActor(amqpConnection: AMQPConnection, gate: String) extends Actor with SerializersWithTypeHints with ActorLogging {
  import context._
  val logger = LoggerFactory.getLogger(this.getClass)
  val producer = ConnectionOwner.createChildActor(amqpConnection.conn, ChannelOwner.props())

//  println("--> RequestPublisherActor created: "+self.toString())
  Amqp.waitForConnection(system, amqpConnection.conn, producer).await(5, TimeUnit.SECONDS)

  override def receive: Receive = {

    case PublishToQueue(correlationId,dm)  =>

      val senderRef = sender()

      dm match {
        case resp: CheckResponse =>
          logger.debug("resp: "+resp)
          val data = write[DomainMessage[_]](dm)

          logger.debug("data "+data)
          publish(
            data = data,
            routingKey = resp.responseKey,
            exchange = gate,
            correlationId = None,
            replyTo = None,
            headers = Some(dm.headers.map(m => (m._1, m._2.asInstanceOf[AnyRef])))
          ) onComplete {
            case Success(Right(ok)) => senderRef ! RequestPublisherActor.Success(correlationId)
            case Success(Left(err)) => senderRef ! RequestPublisherActor.Failure(correlationId, err.reason)
            case Failure(ex) => senderRef ! RequestPublisherActor.Failure(correlationId, ex)
          }
        case req : Request[_] =>

          val data = write[DomainMessage[_]](dm)

          log.debug(data)
          logger.debug("data "+data)
          publish(
            data = data,
            routingKey = req.routingKey,
            exchange = gate,
            correlationId = Some(correlationId),
            replyTo = Some(req.replyTo.get),
            headers = Some(dm.headers.map(m => (m._1, m._2.asInstanceOf[AnyRef])))
          ) onComplete {
            case Success(Right(ok)) => senderRef ! RequestPublisherActor.Success(correlationId)
            case Success(Left(err)) => senderRef ! RequestPublisherActor.Failure(correlationId, err.reason)
            case Failure(ex) => senderRef ! RequestPublisherActor.Failure(correlationId, ex)
          }



        case _ =>
          log.error("Received unsupported message. This actor accepts only PublishToQueue() message")
          throw new RuntimeException("Unsupported message")

    }

  }

  def publish(data            : String,
              routingKey      : String,
              exchange        : String,
              correlationId   : Option[String] = None,
              replyTo         : Option[String] = None,
              messageId       : Option[String] = None,
              `type`          : Option[String] = None,
              contentType     : String = "application/json",
              contentEncoding : String = "UTF-8",
              headers         : Option[Map[String, AnyRef]] = None) : Future[Either[Amqp.Error, Amqp.Ok]] = {

    implicit val timeout: Timeout = 60 seconds

    val body = data.getBytes

    val javaHeaders: java.util.Map[String, AnyRef] = headers match {
      case Some(h) => mapAsJavaMap[String, AnyRef](h)
      case _ => null
    }

    val properties = new AMQP.BasicProperties.Builder()
      .contentType(contentType)
      .contentEncoding(contentEncoding)
      .messageId(messageId.orNull)
      .replyTo(replyTo.orNull)
      .correlationId(correlationId.orNull)
      .`type`(`type`.orNull)
      .headers(javaHeaders)
      .build()

    val res = producer ? Publish(exchange, routingKey, body, Some(properties))

    res.map {
      case m@Amqp.Ok(request, result) => Right(m)
      case m@Amqp.Error(request, reason) => Left(m)
    }

  }

}
