package routing.protocol

import kz.dar.eco.domain.serializers.{DateTimeSerializer, PersonaSerializer, ProfileSerializer, UserContextSerializer}
import org.json4s._
import org.json4s.native.Serialization

/**
 * Created by Madi
 */
trait Serializers extends DateTimeSerializer with ProfileSerializer with PersonaSerializer with UserContextSerializer  {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(NoTypeHints) +
    new DateTimeSerializer() ++
    profileFormats ++
    userContextFormats ++
    personaFormats addKeySerializers
    profileKeyFormats

}
