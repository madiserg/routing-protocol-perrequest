name := "routing-protocol-perRequest"

version := "1.0"

scalaVersion := "2.11.7"

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
resolvers += Resolver.bintrayRepo("hmrc", "releases")

libraryDependencies ++= Dependencies.depends

enablePlugins(JavaAppPackaging)

Revolver.settings