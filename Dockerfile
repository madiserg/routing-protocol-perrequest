#
# Scala and sbt Dockerfile
#
# https://github.com/hseeberger/scala-sbt
#

# Pull base image
FROM 227185325630.dkr.ecr.us-east-1.amazonaws.com/java8-deb:latest

MAINTAINER Madi Sergazin <madi.sergazin@greenapple.kz>

ENV APP_NAME routing-protocol-perrequest-1.0.zip
ENV APP_DIR routing-protocol-perrequest-1.0
ENV JAVA_OPTS -Xms128M -Xmx512M -Xss1M -XX:+CMSClassUnloadingEnabled
ENV RUN_SCRIPT routing-protocol-perrequest
ENV LOG_DIR /eps/logs/routing_protocol
ENV LOG_ARCHIVE_DIR /eps/logs/archive/system/routing_protocol

# logs
RUN mkdir -p /root/config/ \
    && mkdir -p $LOG_DIR \
    && mkdir -p $LOG_ARCHIVE_DIR

COPY ./src/main/resources/*logback.xml /root/config/
COPY ./src/main/resources/*.conf /root/config/

WORKDIR /root
COPY ./target/universal/$APP_NAME /root/
RUN unzip -q $APP_NAME
WORKDIR /root/$APP_DIR/bin
CMD chmod +x $RUN_SCRIPT
EXPOSE 8092
CMD ./$RUN_SCRIPT -Dconfig.resource=/${config}.conf
