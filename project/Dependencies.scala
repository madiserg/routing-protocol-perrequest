import sbt._

object Version {
  val akka            = "2.3.9"
  val spray           = "1.3.3"
  val spray_json      = "1.3.1"
  val scala           = "2.11.6"
  val slick           = "3.0.0"
  val slick_joda      = "2.0.0"
  val h2Driver        = "1.4.185"
  val scalaTest       = "2.2.4"
  val specs2           = "2.4.9"
  val specs2Core      = "2.4.17"
  val typesafeConfig  = "1.3.0"
  val lockback        = "1.1.2"
  val postgres        = "9.3-1100-jdbc41"
  val json4s          = "3.2.10"
  val jodaTime        = "2.8.1"
  val jodaConverter   = "1.7"
  val hikaricp        = "2.3.8"
  val emailValidator  = "0.4.0"
  val amqpClient      = "3.5.3"
  val stoneAmqpClient = "1.5"
  val reactiveMongo   = "0.10.5.0.akka23"
  val gatling         = "2.1.7"
  val jwt             = "0.4.1"
  val couchdb         = "0.5.2"
  val ecoDomain       = "1.0.0"
  val ecoExceptions   = "1.0.0"
  val ecoMessages     = "1.0.0"
}

object Library {
  val akkaActor       = "com.typesafe.akka"    %% "akka-actor"        % Version.akka
  val sprayCan        = "io.spray"             %% "spray-can"         % Version.spray
  val sprayRouting    = "io.spray"             %% "spray-routing-shapeless2"     % Version.spray
  val sprayHttp       = "io.spray"             %% "spray-http"        % Version.spray
  val sprayHttpx      = "io.spray"             %% "spray-httpx"       % Version.spray
  val sprayClient     = "io.spray"             %% "spray-client"      % Version.spray
  val sprayUtil       = "io.spray"             %% "spray-util"        % Version.spray
  val sprayJson       = "io.spray"             %% "spray-json"        % Version.spray_json

  val slick           = "com.typesafe.slick"   %% "slick"             % Version.slick
  val typesafeConfig  = "com.typesafe"         %  "config"            % Version.typesafeConfig
  val slick_joda      = "com.github.tototoshi" %% "slick-joda-mapper" % Version.slick_joda
  val postgres        = "org.postgresql"       %  "postgresql"        % Version.postgres

  val akkaTestKit     = "com.typesafe.akka"    %% "akka-testkit"      % Version.akka  % "test"
  val sprayTestKit    = "io.spray"             %% "spray-testkit"     % Version.spray % "test"
  val specs2Core      = "org.specs2"           %% "specs2-core"       % Version.specs2Core % "test"
  val spec2           = "org.specs2"           %% "specs2"            % Version.specs2 % "test"
  val scalaTest       = "org.scalatest"        %  "scalatest_2.11"    % Version.scalaTest % "test"

  val lockback        = "ch.qos.logback"       %  "logback-classic"   % Version.lockback
  val joda            = "joda-time"            %  "joda-time"         % Version.jodaTime
  val jodaConverter   = "org.joda"             %  "joda-convert"      % Version.jodaConverter
  val json4sNative    = "org.json4s"           %% "json4s-native"     % Version.json4s
  val json4sJackson   = "org.json4s"           %% "json4s-jackson"    % Version.json4s

  val hikaricp        = "com.zaxxer"           %  "HikariCP"          % Version.hikaricp
  val emailValidator  = "uk.gov.hmrc"          %% "emailaddress"      % Version.emailValidator

  val amqpClient      = "com.rabbitmq"         % "amqp-client"        % Version.amqpClient
  val stoneAmqpClient = "com.github.sstone"    % "amqp-client_2.11"   % Version.stoneAmqpClient

  val reactiveMongo   = "org.reactivemongo"    %%  "reactivemongo"     % Version.reactiveMongo

  val gatlingHighcharts  = "io.gatling.highcharts" % "gatling-charts-highcharts" % Version.gatling % "test"
  val gatling         = "io.gatling"            % "gatling-test-framework"    % Version.gatling % "test"
  val jwt             = "com.jason-goodwin"    %% "authentikat-jwt"          % Version.jwt
  val couchdb         = "com.ibm"              %% "couchdb-scala"     % Version.couchdb
  val ecoDomain       = "kz.dar.eco"           %% "dar-eco-domain"    % Version.ecoDomain
  val ecoExceptions   = "kz.dar.eco"           %% "dar-eco-exceptions"  % Version.ecoExceptions
  val ecoMessages     = "kz.dar.eco"           %% "dar-eco-messages"  % Version.ecoMessages

}

object Dependencies {

  import Library._

  val depends = Seq(
     akkaActor,
     sprayCan,
     sprayRouting,
     sprayHttp,
     sprayHttpx,
     sprayClient,
     sprayUtil,
     sprayJson,
     slick,
     typesafeConfig,
     akkaTestKit,
     sprayTestKit,
     spec2,
     specs2Core,
     scalaTest,
     lockback,
     joda,
     jodaConverter,
     postgres,
     slick_joda,
     json4sNative,
     json4sJackson,
     hikaricp,
     emailValidator,
     amqpClient,
     stoneAmqpClient,
//     reactiveMongo,
//     gatling,
//     gatlingHighcharts,
     jwt,
     couchdb,
    ecoDomain,
    ecoExceptions,
    ecoMessages
  )
}
